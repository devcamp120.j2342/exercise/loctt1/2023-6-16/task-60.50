package com.jpacarrepository.jpacar.models;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "car")
public class CCar {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int car_id;

    @Column(name = "car_code")
    private String carCode;

    @Column(name = "car_name")
    private String carName;

    @OneToMany(mappedBy = "car", cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<CCarType> types;
    public CCar() {
    }
    public CCar(int car_id, String carCode, String carName, Set<CCarType> types) {
        this.car_id = car_id;
        this.carCode = carCode;
        this.carName = carName;
        this.types = types;
    }
    public int getCar_id() {
        return car_id;
    }
    public void setCar_id(int car_id) {
        this.car_id = car_id;
    }
    public String getCarCode() {
        return carCode;
    }
    public void setCarCode(String carCode) {
        this.carCode = carCode;
    }
    public String getCarName() {
        return carName;
    }
    public void setCarName(String carName) {
        this.carName = carName;
    }
    public Set<CCarType> getTypes() {
        return types;
    }
    public void setTypes(Set<CCarType> types) {
        this.types = types;
    }
    
    
}

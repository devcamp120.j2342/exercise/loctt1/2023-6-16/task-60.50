package com.jpacarrepository.jpacar.models;

import com.fasterxml.jackson.annotation.JsonBackReference;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "carType")
public class CCarType {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int type_id;

    @Column(name = "type_code")
    private String typeCode;

    @Column(name = "type_name")
    private String typeName;

    @ManyToOne
    @JoinColumn(name = "car_id" )
    @JsonBackReference
    private CCar car;

    public CCarType() {
    }

    public CCarType(int type_id, String typeCode, String typeName, CCar car) {
        this.type_id = type_id;
        this.typeCode = typeCode;
        this.typeName = typeName;
        this.car = car;
    }

    public int getType_id() {
        return type_id;
    }

    public void setType_id(int type_id) {
        this.type_id = type_id;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public CCar getCar() {
        return car;
    }

    public void setCar(CCar car) {
        this.car = car;
    }

    
    
}

package com.jpacarrepository.jpacar.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jpacarrepository.jpacar.models.CCarType;

public interface ICarTypeRepository extends JpaRepository<CCarType, Long>{
    
}

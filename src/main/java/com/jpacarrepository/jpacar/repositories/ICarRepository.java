package com.jpacarrepository.jpacar.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jpacarrepository.jpacar.models.CCar;

public interface ICarRepository extends JpaRepository<CCar, Long> {
    CCar findByCarCode(String carCode);
}

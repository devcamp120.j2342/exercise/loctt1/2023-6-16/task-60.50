package com.jpacarrepository.jpacar.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jpacarrepository.jpacar.models.CCar;
import com.jpacarrepository.jpacar.models.CCarType;
import com.jpacarrepository.jpacar.repositories.ICarRepository;
import com.jpacarrepository.jpacar.repositories.ICarTypeRepository;

@CrossOrigin(value = "*", maxAge = -1)
@RestController
@RequestMapping("/")
public class CCarController {
    @Autowired
    ICarRepository pCarRepository;

    @Autowired
    ICarTypeRepository pCarTypeRepository;
    
    @GetMapping("/devcamp-cars")
    public ResponseEntity<List<CCar>> getAllCars() {
       
        try {
            List<CCar> listCars = new ArrayList<CCar>();
            pCarRepository.findAll().forEach(listCars::add);
            return new ResponseEntity<>(listCars, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/devcamp-cartypes")
    public ResponseEntity<Set<CCarType>> getCarTypeByCarCode(@RequestParam(value = "carCode")String carCode) {
        try {
            CCar vCar = pCarRepository.findByCarCode(carCode);

            if (vCar != null) {
                return new ResponseEntity<>(vCar.getTypes(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
